const { limitFunctionCallCount } = require("../limitFunctionCallCount");

const callBack = (counter) => {
  console.log(counter + " times callback function called");
};

let res = limitFunctionCallCount(callBack, 3);
res();
res()
res()
res()

