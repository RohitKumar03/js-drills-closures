const { counterFactory } = require("../counterFactory.js");

let res = counterFactory();
console.log(res.increment());
console.log(res.decrement());
