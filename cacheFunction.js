function cacheFunctions(cb) {
  let cache = {};
  function inner(num) {
    if (num in cache) {
      console.log("Below is the cached result");
      return cache[num];
    } else {
      cache[num] = cb(num);

      return cache[num];
    }
  }
  return inner;
}

module.exports.cacheFunctions = cacheFunctions;
