function limitFunctionCallCount(cb, n) {
  let count = 0;
  function counterFunc() {
    if (count < n) {
        count++;
      cb(count);
     
    }
  }
  return counterFunc;
}

module.exports.limitFunctionCallCount = limitFunctionCallCount;
